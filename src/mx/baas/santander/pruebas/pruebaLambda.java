package mx.baas.santander.pruebas;

import mx.baas.santander.interfaces.IPruebaLambda;

public class pruebaLambda {
	public static void main(String args[]) {
		new pruebaLambda().something();
	}
	
	public void something() {
		IPruebaLambda prueba = s->System.out.println("hola " + s);
		prueba.getSaludo("Gabriel");
	}
}

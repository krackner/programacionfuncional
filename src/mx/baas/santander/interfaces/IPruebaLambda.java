package mx.baas.santander.interfaces;

@FunctionalInterface
public interface IPruebaLambda {
	void getSaludo(String name);
}
